#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script adds a CV tier for each participant in an ELAN file, based on an XV tier.
The XV tier is assumed to be individual words broken up into separate annotations. 
Hyphens and null null symbols (ø) are removed.
"""
import sys
import codecs
import re
import os
from argparse import ArgumentParser
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from xml.dom import minidom

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__)
	parser.add_argument('eaf', help = 'ELAN file')
	opts = parser.parse_args()
	
	# Using ElementTree to parse ELAN file, setting up all variables.
	tree = ET.parse(open(opts.eaf, 'r'))
	root = tree.getroot()
	annotationIDs = []
	typeTier = False
	for element in root.iter('ANNOTATION_DOCUMENT'):
		ANNOTATION_DOCUMENT = element

	# Adding linguistic type information for cv tier if it's not already there
	for element in root.iter('LINGUISTIC_TYPE'):
		if element.attrib['LINGUISTIC_TYPE_ID'] == "cv":
			typeTier = True
			break
	if not typeTier:
		attribs = {"CONSTRAINTS":"Symbolic_Association",
					"GRAPHIC_REFERENCES":"false",
					"LINGUISTIC_TYPE_ID":"cv",
					"TIME_ALIGNABLE":"false"}
		for element in root.iter('ANNOTATION_DOCUMENT'):
			ET.SubElement(element,"LINGUISTIC_TYPE",attribs)
	
	# Making a list of all the annotation ID numbers used in the file
	for element in root.iter('REF_ANNOTATION'):
		if element.attrib['ANNOTATION_ID']:
			n = int(re.search(r'an*([0-9]+)', element.attrib['ANNOTATION_ID']).group(1))
			annotationIDs.append(n)
	for element in root.iter('ALIGNABLE_ANNOTATION'):
		if element.attrib['ANNOTATION_ID']:
			n = int(re.search(r'an*([0-9]+)', element.attrib['ANNOTATION_ID']).group(1))
			annotationIDs.append(n)

	# Looking on each xv tier
	for element in root.iter('TIER'):
		if element.attrib['LINGUISTIC_TYPE_REF'] == "xv":
			utterances = {}
			parentref = ""
			participant = ""
			initials = ""
			cvtierid = ""
			newID = ""
			# Setting up tier ID information
			if element.attrib['PARENT_REF']:
				parentref = element.attrib['PARENT_REF']
				if "@" in parentref and bool(re.search(r'@(.+)', parentref)):
					initials = re.search(r'@(.+)', parentref).group(1)
			if element.attrib['PARTICIPANT']:
				participant = element.attrib['PARTICIPANT']
			if element.attrib['TIER_ID']:
				tierid = element.attrib['TIER_ID']
				if "@" in tierid and bool(re.search(r'@(.+)', tierid)):
					tierid = re.search(r'@(.+)', tierid).group(1)
			if initials:
				cvtierid = "cv@"+initials
			
			# Adding each word to utterance dictionary
			for subelement in element.iter('REF_ANNOTATION'):
				if subelement.attrib['ANNOTATION_REF'] not in utterances:
					utterances[subelement.attrib['ANNOTATION_REF']] = []
				for subsubelement in subelement.iter('ANNOTATION_VALUE'):
					if subsubelement.text:
						word = subsubelement.text.replace(u'ø','') # Removing hyphens and ø's
						word = word.replace('-','')
						utterances[subelement.attrib['ANNOTATION_REF']].append(word)

			# Making CV tier, should be one for each participant
			newtier = ET.SubElement(ANNOTATION_DOCUMENT,
				"TIER",
				{"DEFAULT_LOCALE":"en",
				"LINGUISTIC_TYPE_REF":"cv",
				"PARENT_REF":parentref,
				"PARTICIPANT":participant,
				"TIER_ID":cvtierid})
			
			for element in root.iter('TIER'):
				if element.attrib['TIER_ID'] == cvtierid:
					for annotation_ref in sorted(utterances):
						# Making unique annotation ID number
						for n in range(1,100000):
							if n not in annotationIDs:
								newID = "a"+str(n)
								annotationIDs.append(n)
								break

						# Setting up element structure for each utterance
						ET.SubElement(
							ET.SubElement(
							ET.SubElement(newtier,'ANNOTATION'),
								'REF_ANNOTATION',
								{'ANNOTATION_ID':newID,
								'ANNOTATION_REF':annotation_ref}),
							'ANNOTATION_VALUE').text = " ".join(utterances[annotation_ref])

	# Output = file name plus _CV
	output = os.path.basename(opts.eaf).replace(".eaf", "_CV.eaf")
	tree.write(output, encoding='UTF-8')

	# Pretty xml - don't use as it adds linebreaks to annotations
	#xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="")
	#with open (output, 'w') as f:
	#	f.write(xmlstr.encode('utf-8'))


if __name__=="__main__":
	main()
